/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package job;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.MutableDateTime;

/**
 *
 * @author Davide
 */
public class Job {

    public static String DATABASE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final int JOB_REGISTRAZIONE_T_2 = 2;
    public static final int JOB_REGISTRAZIONE_T_6 = 6;
    public static final int JOB_REGISTRAZIONE_T_7 = 7;

    public static final int JOB_SCONTO_T_7 = 7;
    public static final int JOB_SCONTO_T_LESS_3 = 3;
    public static final int JOB_SCONTO_T_LESS_1 = 1;
    public static final int JOB_SCONTO_T_LESS_0 = 0;

    public static void doJobRegistration(int t, String titolo, String messaggio) {
        try {
            Connection connessioneDB = DBConnection.getConnection();
            DateTime oggi = new DateTime(new Date());
            String query = "SELECT * FROM utente INNER JOIN utente_device ON id_utente = utente.id WHERE 1=1";
            ResultSet resultSet = connessioneDB.createStatement().executeQuery(query);
            NotificationManager notificationManager = new NotificationManager();
            notificationManager.startSessione();
            while (resultSet.next()) {

                try {
                    String dataRegistrazioneUtente = resultSet.getString("registrato_il");
                    String token = resultSet.getString("token");
                    int tipoDispositivo = resultSet.getInt("tipo_dispositivo");
                    int selfieCoin = resultSet.getInt("selfie_coin");
                    String nome = resultSet.getString("nome");
                    if (token != null && dataRegistrazioneUtente != null) {
                        Date d = new SimpleDateFormat(DATABASE_DATE_FORMAT).parse(dataRegistrazioneUtente);
                        DateTime dataRegistrazione = new DateTime(d);
                        MutableDateTime dataRegistrazioneMutable = dataRegistrazione.toMutableDateTime();
                        dataRegistrazioneMutable.setHourOfDay(0);
                        dataRegistrazioneMutable.setMinuteOfHour(0);
                        dataRegistrazioneMutable.setSecondOfMinute(0);
                        dataRegistrazione = dataRegistrazioneMutable.toDateTime();
                        int days = Days.daysBetween(dataRegistrazione, oggi).getDays();
                        if (days == t && selfieCoin >= 500) {
                            if (!messaggio.isEmpty() && !token.isEmpty()) {
                                if (messaggio.contains("#SELFIECOIN")) {
                                    messaggio = messaggio.replace("#SELFIECOIN", selfieCoin + "");
                                }
                                if (messaggio.contains("#UTENTE")) {
                                    messaggio = messaggio.replace("#UTENTE", nome);
                                }
                                if (messaggio.contains("#PERC")) {
                                    messaggio = messaggio.replace("#PERC", "%");
                                }
                                if (titolo.contains("#SELFIECOIN")) {
                                    titolo = titolo.replace("#SELFIECOIN", selfieCoin + "");
                                }
                                if (titolo.contains("#UTENTE")) {
                                    titolo = titolo.replace("#UTENTE", nome);
                                }
                                if (titolo.contains("#PERC")) {
                                    titolo = titolo.replace("#PERC", "%");
                                }
                                if (tipoDispositivo == 1) {
                                    notificationManager.inviaNotificaiOS(token, titolo, messaggio, Costanti.ACTION_HOME_VIEW, 0);
                                } else if (tipoDispositivo == 2) {
                                    notificationManager.inviaNotificaAndroid(token, titolo, messaggio, Costanti.ACTION_HOME_VIEW, 0);
                                }
                            }
                        }
                    }
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                    Logger.log(ex.getMessage());
                }
            }
            notificationManager.pulisciDatabase();
            notificationManager.stampaReport();
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.log(ex.getMessage());
        }
    }

    public static void doJobScontoScontrino(int t, int[] categorie, String titolo, String messaggio) {
        try (Connection connessioneDB = DBConnection.getConnection()) {
            DateTime oggi = new DateTime(new Date());
            String oggiMySQL = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            String query = "SELECT sconto.*, utente_device.token, utente_device.tipo_dispositivo, esercente.nome, esercente.id_categoria FROM sconto "
                    + "INNER JOIN utente_device ON sconto.id_utente = utente_device.id_utente "
                    + "INNER JOIN (esercente INNER JOIN categoria ON categoria.id = esercente.id_categoria) "
                    + "ON esercente.id = sconto.id_esercente WHERE convalidato = 0 AND data_scadenza >= ? ";
            if (categorie != null) {
                String queryCategories = "";
                for (int id : categorie) {
                    if (!queryCategories.isEmpty()) {
                        queryCategories += " OR ";
                    }
                    queryCategories += " id_categoria = " + id;
                }
                if (!queryCategories.isEmpty()) {
                    query = query + " AND (" + queryCategories + ")";
                }
            }
            PreparedStatement preparedStatement = connessioneDB.prepareStatement(query);
            preparedStatement.setString(1, oggiMySQL);
            ResultSet resultSet = preparedStatement.executeQuery();
            NotificationManager notificationManager = new NotificationManager();
            notificationManager.startSessione();
            while (resultSet.next()) {
                String dataCreazioneSconto = resultSet.getString("data_creazione");
                String dataScadenzaSconto = resultSet.getString("data_scadenza");
                String nomeEsercente = resultSet.getString("nome");
                String token = resultSet.getString("token");
                int tipoDispositivo = resultSet.getInt("tipo_dispositivo");
                int sconto = resultSet.getInt("sconto");
                int idEsercente = resultSet.getInt("id_esercente");
                if (token != null) {
                    Date d = new SimpleDateFormat(DATABASE_DATE_FORMAT).parse(dataCreazioneSconto);
                    Date d2 = new SimpleDateFormat(DATABASE_DATE_FORMAT).parse(dataScadenzaSconto);
                    DateTime dataCreazione = new DateTime(d);
                    DateTime dataScadenza = new DateTime(d2);
                    MutableDateTime dataCreazioneMutable = dataCreazione.toMutableDateTime();
                    MutableDateTime dataScadenzaMutable = dataScadenza.toMutableDateTime();
                    dataCreazioneMutable.setHourOfDay(0);
                    dataCreazioneMutable.setMinuteOfHour(0);
                    dataCreazioneMutable.setSecondOfMinute(0);
                    dataScadenzaMutable.setHourOfDay(23);
                    dataScadenzaMutable.setMinuteOfHour(59);
                    dataScadenzaMutable.setSecondOfMinute(59);
                    dataCreazione = dataCreazioneMutable.toDateTime();
                    dataScadenza = dataScadenzaMutable.toDateTime();
                    int dayCreazione = Days.daysBetween(dataCreazione, oggi).getDays();
                    int dayScadenza = Days.daysBetween(dataScadenza, oggi).getDays();

                    if ((t == dayCreazione && t != 0)|| t == dayScadenza) {
                        if (!messaggio.isEmpty() && !token.isEmpty()) {
                            if (messaggio.contains("#ESERCENTE")) {
                                messaggio = messaggio.replace("#ESERCENTE", nomeEsercente);
                            }
                            if (messaggio.contains("#SCONTO")) {
                                messaggio = messaggio.replace("#SCONTO", sconto + "");
                            }
                            if (messaggio.contains("#GIORNI")) {
                                messaggio = messaggio.replace("#GIORNI", t + "");
                            }
                            if (messaggio.contains("#PERC")) {
                                    messaggio = messaggio.replace("#PERC", "%");
                            }
                            if (titolo.contains("#ESERCENTE")) {
                                titolo = titolo.replace("#ESERCENTE", nomeEsercente);
                            }
                            if (titolo.contains("#SCONTO")) {
                                titolo = titolo.replace("#SCONTO", sconto + "");
                            }
                            if (titolo.contains("#GIORNI")) {
                                titolo = titolo.replace("#GIORNI", t + "");
                            }
                            if (titolo.contains("#PERC")) {
                                    titolo = titolo.replace("#PERC", "%");
                            }
                            if (tipoDispositivo == 1) {
                                notificationManager.inviaNotificaiOS(token, titolo, messaggio, Costanti.ACTION_MERCHANT_VIEW, idEsercente);
                            } else if (tipoDispositivo == 2) {
                                notificationManager.inviaNotificaAndroid(token, titolo, messaggio, Costanti.ACTION_MERCHANT_VIEW, idEsercente);
                            }
                        }
                    }
                }
            }
            notificationManager.pulisciDatabase();
            notificationManager.stampaReport();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            Logger.log(ex.getMessage());
        }
    }
}
