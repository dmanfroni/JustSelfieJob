/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package job;

/**
 *
 * @author Davide
 */
public class JOBNotification {

    
    public static final String JOB_REGISTRAZIONE = "registrazione";
    public static final String JOB_SCONTO_SCONTRINO = "sconto_scontrino";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            Logger.log(Messages.EMPTY);
        }else{
            
            if(args[0].matches(JOB_REGISTRAZIONE)){
                Job.doJobRegistration(Integer.parseInt(args[1]), args[2], args[3]);
                return;
            }
            if(args[0].matches(JOB_SCONTO_SCONTRINO)){
                int t = Integer.parseInt(args[1]);
                int[] categorie = null;
                if(!args[2].matches("-1")){
                    String[] catArray = args[2].split(",");
                    categorie = new int[catArray.length];
                    for(int i = 0; i < catArray.length; i++){
                        categorie[i] = Integer.parseInt(catArray[i]);
                    }
                }
                Job.doJobScontoScontrino(t, categorie, args[3], args[4]);
            }
        }
    }

}
