/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package job;

/**
 *
 * @author Davide
 */
public class Messages {

    public static final String REGISTRAZIONE_T_2 = "Ora di pranzo...Fai un break! Dal panino gourmet, al maxi toast, al menu bio veg sono tante le possibilita con sconti fino al 60% + bonus da 5€ Extra. Avvia l'app!";
    public static final String REGISTRAZIONE_T_6 = "I-M-P-E-R-D-I-B-I-L-E: da Peacock l'aperitivo con buffet lo paghi solo 1€ grazie all'offerta di benvenuto in app + sconto extra di 5€, approfittane ora!";
    public static final String REGISTRAZIONE_T_7 = "Stasera PIZZA!\nOggi sconto super: menu completo con antipasto, pizza, acqua e caffè a soli 5€ all'Ostera la Matta! Avvia l'app!";
    public static final String SCONTO_T_7 = "Stasera PIZZA!\nOggi sconto super: menu completo con antipasto, pizza, acqua e caffè a soli 5€ all'Ostera la Matta! Avvia l'app!";
    public static final String SCONTO_T_LESS_3 = "Stasera PIZZA!\nOggi sconto super: menu completo con antipasto, pizza, acqua e caffè a soli 5€ all'Ostera la Matta! Avvia l'app!";
    public static final String SCONTO_T_LESS_1 = "Stasera PIZZA!\nOggi sconto super: menu completo con antipasto, pizza, acqua e caffè a soli 5€ all'Ostera la Matta! Avvia l'app!";
    public static final String SCONTO_T_LESS_0 = "Stasera PIZZA!\nOggi sconto super: menu completo con antipasto, pizza, acqua e caffè a soli 5€ all'Ostera la Matta! Avvia l'app!";

    public static final String EMPTY = "Job vuoto";
}
