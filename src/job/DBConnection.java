package job;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author dmanfroni
 */
public class DBConnection {
    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Connection con = null;
        Class.forName(Costanti.DB_DRIVER_CLASS);
        con = DriverManager.getConnection(Costanti.DB_URL + Costanti.DB_NAME, Costanti.DB_USERNAME, Costanti.DB_PASSWORD);
        return con;
    }
}
