/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package job;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

/**
 *
 * @author Davide
 */
public class Logger {

    public static void log(String log) {
        FileWriter filerWriter = null;
        try {
            File file = new File(Costanti.REPORT_FILE);
            //filerWriter = new FileWriter(file);
            StringBuilder stringBuilder = new StringBuilder();
            
            stringBuilder.append("#").append(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date())).append(": ");
            stringBuilder.append(log);
            stringBuilder.append("\n");
             Files.write(Paths.get(Costanti.REPORT_FILE), stringBuilder.toString().getBytes(), StandardOpenOption.APPEND);
            //filerWriter.flush();
            //filerWriter.close();
        } 
        catch (IOException ex) {
        System.out.println(ex.getMessage());
        
        }
    }

}
